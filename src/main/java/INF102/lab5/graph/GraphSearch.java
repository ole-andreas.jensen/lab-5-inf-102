package INF102.lab5.graph;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;
    private Set<V> explored;
    private TreeSet<V> path;
    private Set<V> remaining;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
        reset();
    }

    private void reset() {
        this.explored = new TreeSet<>();
        this.path = new TreeSet<>();
        this.remaining = new TreeSet<>();
        this.remaining.addAll(graph.getNodes());
    }

    @Override
    public boolean connected(V u, V v) {
        explored.add(u);
        path.add(u);
        remaining.remove(u);
        if (u.equals(v)) {
            reset();
            return true;
        }

        while (allNeighboursFound(path.last())) {
            if (path.size() == 1) {
                reset();
                return false;
            }
            path.remove(path.last());
        }

        Set<V> neighbours = graph.getNeighbourhood(path.last());
        for (V node : neighbours) {
            if (explored.contains(node)) {
                continue;
            }
            return connected(node, v);
        }
        return false;
    }

    private boolean allNeighboursFound(V u) {
        for (V node : graph.getNeighbourhood(u)) {
            if (!explored.contains(node)) {
                return false;
            }
        }
        return true;
    }

}
